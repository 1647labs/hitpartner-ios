//
//  AWSAuth.swift
//  HitParters
//
//  Created by Matt on 8/17/18.
//  Copyright © 2018 Matt. All rights reserved.
//

import UIKit
import AWSCognitoIdentityProvider
import OneSignal

class IdentityManager: NSObject {
    static let sharedManager = IdentityManager()
    private var userPool: AWSCognitoIdentityUserPool?
    private var configuration: AWSServiceConfiguration?
    public private(set) var currentHPUser: HPUser?
    
    func configure(pool: AWSCognitoIdentityUserPool, config: AWSServiceConfiguration) {
        userPool = pool
        configuration = config
    }
    
    public func authHeader() -> String? {
        let sessionTask = userPool?.currentUser()?.getSession()
        return sessionTask?.result?.idToken?.tokenString
    }
    
    public func currentUser() -> AWSCognitoIdentityUser? {
        return userPool?.currentUser()
    }
    
    public func updateUser(_ user: HPUser?) {
        currentHPUser = user
        signOneSignal(user)
    }
    
    private func signOneSignal(_ user: HPUser?) {
        if let email = user?.email {
            OneSignal.setEmail(email)
        }
        else {
            OneSignal.logoutEmail()
        }
    }
    
}
