/*
 Copyright 2010-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.

 Licensed under the Apache License, Version 2.0 (the "License").
 You may not use this file except in compliance with the License.
 A copy of the License is located at

 http://aws.amazon.com/apache2.0

 or in the "license" file accompanying this file. This file is distributed
 on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 express or implied. See the License for the specific language governing
 permissions and limitations under the License.
 */
 

import Foundation
import AWSCore
 
public class HPUser : AWSModel {
    
    @objc var email: String?
    @objc var firstname: String?
    @objc var lastname: String?
    @objc var address: String?
    @objc var phone_number: String?
    @objc var city: String?
    @objc var state: String?
    @objc var zipcode: String?
    @objc var latitude: String?
    @objc var longitude: String?
    @objc var username: String?
    @objc var hp_level: String?
    @objc var profile_id: String?
    
   	public override static func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]!{
		var params:[AnyHashable : Any] = [:]
		params["firstname"] = "firstname"
		params["lastname"] = "lastname"
        params["email"] = "email"
        params["address"] = "address"
        params["phone_number"] = "phone_number"
        params["city"] = "city"
        params["state"] = "state"
        params["zipcode"] = "zipcode"
        params["latitude"] = "latitude"
        params["longitude"] = "longitude"
        params["username"] = "username"
        params["hp_level"] = "hp_level"
        params["profile_id"] = "profile_id"

        return params
	}
}
