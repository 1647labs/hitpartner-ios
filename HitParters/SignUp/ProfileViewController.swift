//
// Copyright 2014-2018 Amazon.com,
// Inc. or its affiliates. All Rights Reserved.
//
// Licensed under the Amazon Software License (the "License").
// You may not use this file except in compliance with the
// License. A copy of the License is located at
//
//     http://aws.amazon.com/asl/
//
// or in the "license" file accompanying this file. This file is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, express or implied. See the License
// for the specific language governing permissions and
// limitations under the License.
//

import Foundation
import AWSCognitoIdentityProvider
import AWSAPIGateway
import AWSCore
import SVProgressHUD

class ProfileViewController : BaseViewController, UITextFieldDelegate {
    
    var response: AWSCognitoIdentityUserGetDetailsResponse?
    var isEdit = false
    var cognitoUserAttr: [AWSCognitoIdentityProviderAttributeType]?
    let scrollOffsetDeafult: CGFloat = 0
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet var textFields: [UITextField]!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastnameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var addressField: UITextField!
    @IBOutlet weak var cityField: UITextField!
    @IBOutlet weak var stateField: UITextField!
    @IBOutlet weak var zipcodeField: UITextField!
    @IBOutlet weak var skillField: UITextField!
    
    @IBOutlet weak var scrollOffset: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshUser()
        updateUi()
    }
    
    private func updateUi() {
        let _ = textFields.map { $0.delegate = self }
        let _  = textFields.map { $0.isUserInteractionEnabled = isEdit }
        self.navigationItem.rightBarButtonItem = editButton()
    }
    
    private func editButton() -> UIBarButtonItem {
        if !isEdit {
            return UIBarButtonItem(title: "Edit", style: .done, target: self, action: #selector(didTapEdit))
        }
        else {
            return UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(didTapSave))
        }
    }
    
    @objc private func didTapEdit() {
        isEdit = true
        updateUi()
    }
    
    @objc private func didTapSave() {
        isEdit = false
        updateUser()
        
        for field in textFields {
            if field.text == nil  || field.text!.isEmpty {
                self.showError(title: "Missing Fields", message: "Please fill all fields")
                return
            }
        }
        updateUi()
        saveUser()
    }
    
    // MARK: - Profile Management
    
    func refreshUser() {
        //TODO: get email address from aws cognito
        IdentityManager.sharedManager.currentUser()?.getDetails().continueWith(block: { task in
            if let attr = task.result?.userAttributes {
                self.cognitoUserAttr = attr
                let eAttr = attr.filter { $0.name == "email"}.first
                if let email = eAttr?.value {
                    
                    DispatchQueue.main.async {
                        let _ = HPProfileClient.default().getProfile(email:email).continueWith(block: { task in
                            if let u = task.result {
                                IdentityManager.sharedManager.updateUser(u)
                            }
                            DispatchQueue.main.async {
                                self.populateUserData()
                            }
                            return nil
                        })
                    }
                }
                
            }
            return nil
        })
    }
    
    func populateUserData() {
        if let user = IdentityManager.sharedManager.currentHPUser {
            firstNameField.text = user.firstname
            lastnameField.text = user.lastname
            emailField.text = user.email
            phoneField.text = user.phone_number
            addressField.text = user.address
            cityField.text = user.city
            stateField.text = user.state
            zipcodeField.text = user.zipcode
            skillField.text = user.hp_level
        }
         if let ua = cognitoUserAttr {
            emailField.text = ua.filter { $0.name == "email"}.first?.value
            phoneField.text = ua.filter { $0.name == "phone_number"}.first?.value
        }
    }
    
    func updateUser() {
        
        let user = HPUser()
        user?.email = emailField.text
        user?.username = cognitoUserAttr?.filter { $0.name == "sub" }.first?.value
        user?.profile_id = user?.username
        user?.firstname = firstNameField.text
        user?.lastname = lastnameField.text
        user?.phone_number = phoneField.text
        user?.address = addressField.text
        user?.city = cityField.text
        user?.state = stateField.text
        user?.zipcode = zipcodeField.text
        user?.longitude = "0.00"
        user?.latitude = "0.00"
        user?.hp_level = skillField.text
        
        IdentityManager.sharedManager.updateUser(user!)
      
    }
    
    func saveUser() {
        if let user = IdentityManager.sharedManager.currentHPUser {
            SVProgressHUD.show(withStatus: "Saving...")
            let _ = HPProfileClient.default().putProfile(user).continueWith { task in
                if let e = task.error {
                    //TODO: Handle error
                    self.showError(title: "Error", message: e.localizedDescription)
                }
                SVProgressHUD.dismiss()
                return nil
            }
        }
    }
    
    
    // MARK: - Keyboard
    
    override func keyboardWillShow(notification: NSNotification) {
       
            self.scrollOffset.constant = 170
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
    }
    
    override func keyboardWillHide(notification: NSNotification) {
        scrollOffset.constant = scrollOffsetDeafult
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let i = textFields.index(of: textField)
        if i! < textFields.count - 1{
            textFields[i!+1].becomeFirstResponder()
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
     
        scrollView.scrollRectToVisible(textField.frame, animated: true)
    }
    
    
}

