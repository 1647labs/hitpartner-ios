//
// Copyright 2014-2018 Amazon.com,
// Inc. or its affiliates. All Rights Reserved.
//
// Licensed under the Amazon Software License (the "License").
// You may not use this file except in compliance with the
// License. A copy of the License is located at
//
//     http://aws.amazon.com/asl/
//
// or in the "license" file accompanying this file. This file is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, express or implied. See the License
// for the specific language governing permissions and
// limitations under the License.
//

import Foundation
import AWSCognitoIdentityProvider
import SVProgressHUD

class SignUpViewController: BaseViewController {
    
    var pool: AWSCognitoIdentityUserPool?
    var sentTo: String?
    let testData = false
    
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pool = AWSCognitoIdentityUserPool.init(forKey: AWSCognitoUserPoolsSignInProviderKey)
        if testData {
            insertTestData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.confirmPassword.becomeFirstResponder()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let signUpConfirmationViewController = segue.destination as? ConfirmSignUpViewController {
            signUpConfirmationViewController.sentTo = self.sentTo
            signUpConfirmationViewController.user = self.pool?.getUser(self.email.text!)
        }
    }
    
    @IBAction func signUp(_ sender: AnyObject) {
        
       guard let emailValue = self.email.text, !emailValue.isEmpty else {
            showError(title: "Missing Field", message: "Email is required for registration.")
            return
        }
        
        guard let passwordValue = self.password.text, !passwordValue.isEmpty else {
            showError(title: "Missing Field", message: "Password is required for registration.")
            return
        }
        
        guard let cPasswordValue = self.confirmPassword.text, !cPasswordValue.isEmpty, passwordValue == cPasswordValue else {
            showError(title: "Incorrect Fields", message: "Password confirmation does not match.")
            return
        }
        
        guard let phoneValue = self.phone.text, !phoneValue.isEmpty else {
            showError(title: "Missing Field", message: "Phone Number is required for registration.")
            return
        }
        
        var attributes = [AWSCognitoIdentityUserAttributeType]()
        
        if let phoneValue = self.phone.text, !phoneValue.isEmpty {
            let phone = AWSCognitoIdentityUserAttributeType()
            phone?.name = "phone_number"
            phone?.value = phoneValue
            attributes.append(phone!)
        }
        
        if let emailValue = self.email.text, !emailValue.isEmpty {
            let email = AWSCognitoIdentityUserAttributeType()
            email?.name = "email"
            email?.value = emailValue
            attributes.append(email!)
        }
        
        
        SVProgressHUD.show()
        //sign up the user
        self.pool?.signUp(email.text!, password: passwordValue, userAttributes: attributes, validationData: nil).continueWith {[weak self] (task) -> Any? in
            guard let strongSelf = self else { return nil }
            DispatchQueue.main.async(execute: {
                if let error = task.error as NSError? {
                    let alertController = UIAlertController(title: error.userInfo["__type"] as? String,
                                                            message: error.userInfo["message"] as? String,
                                                            preferredStyle: .alert)
                    let retryAction = UIAlertAction(title: "Retry", style: .default, handler: nil)
                    alertController.addAction(retryAction)
                    
                    self?.present(alertController, animated: true, completion:  nil)
                } else if let result = task.result  {
                    // handle the case where user has to confirm his identity via email / SMS
                    if (result.user.confirmedStatus != AWSCognitoIdentityUserStatus.confirmed) {
                        strongSelf.sentTo = result.codeDeliveryDetails?.destination
                        strongSelf.performSegue(withIdentifier: "confirmSignUpSegue", sender:sender)
                    } else {
                        let _ = strongSelf.navigationController?.popToRootViewController(animated: true)
                    }
                }
                SVProgressHUD.dismiss()
            })
            return nil
        }
    }
    
    func insertTestData() {
        self.email.text = "matt.pis+2@yahoo.com"
        self.password.text = "Password123."
        self.confirmPassword.text = "Password123."
        self.phone.text = "+17737873052"
    }
}
