//
//  SettingsViewController.swift
//  HitParters
//
//  Created by Matt on 8/9/18.
//  Copyright © 2018 Matt. All rights reserved.
//

import UIKit
import AWSCognitoIdentityProvider

class SettingsViewController: UIViewController {

    var user: AWSCognitoIdentityUser?
    var pool: AWSCognitoIdentityUserPool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pool = AWSCognitoIdentityUserPool(forKey: AWSCognitoUserPoolsSignInProviderKey)
        if (self.user == nil) {
            self.user = self.pool?.currentUser()
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signoutTapped(_ sender: Any) {
      
        self.user?.signOut()
        self.user?.getDetails()
        IdentityManager.sharedManager.updateUser(nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
