//
// Copyright 2014-2018 Amazon.com,
// Inc. or its affiliates. All Rights Reserved.
//
// Licensed under the Amazon Software License (the "License").
// You may not use this file except in compliance with the
// License. A copy of the License is located at
//
//     http://aws.amazon.com/asl/
//
// or in the "license" file accompanying this file. This file is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, express or implied. See the License
// for the specific language governing permissions and
// limitations under the License.
//

import Foundation
import AWSCognitoIdentityProvider

let CognitoIdentityUserPoolRegion: AWSRegionType = .USEast1
let CognitoIdentityUserPoolId = "us-east-1_yjNQNdvgk"
let CognitoIdentityUserPoolAppClientId = "4q29cetgtopa100pllu2g0jc02"
let CognitoIdentityUserPoolAppClientSecret = "32l8305945fcr3is0r7hc0g3p1pgtvnum8kf0okcfd3rqkjib85"

let AWSCognitoUserPoolsSignInProviderKey = "UserPool"

let OneSignalAppId = "412b74c5-13e7-42e4-ab58-95f08080c1d7"
